# openproject 

OpenProject is the leading open source project management software. 

## First test (without database) : 

docker run -it -p 8080:80 -e SECRET_KEY_BASE=testing123 openproject/community:latest


## sources :

- openproject github page : https://github.com/opf/openproject
- openproject installation doc : https://www.openproject.org/docs/installation-and-operations/installation/docker/