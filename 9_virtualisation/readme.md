# Lab 9 "Virtualisation" 

Lab de test de solution de virtualisations

## Lab local 

### Virtulisation système

#### podman

- [TODO] créer un serveur distant pour la machine virtuelle podman utilisée pour mac os : https://luiscachog.io/podman-macos-vagrant/
- [TODO] monter sa propre machine virtuelle avec multipass (Hyperkit), à la place de virtualbox (par défaut) : https://github.com/hutger/podman-on-mac
- [TODO] créer un repo local pour les images podman : https://thenewstack.io/tutorial-host-a-local-podman-image-registry/

#### docker 

- set docker remote host
- install docker as rootless daemon

#### KVM (quemu)

- [TODO] KVM + Proxmox

#### LXC

- [TODO] LXC + Proxmox

#### Xen

#### virtualbox 

### Virtualisation réseau 

#### GNS3

- [TODO] lab VPN et pare-feu avec GNS3 : https://openclassrooms.com/fr/courses/1946106-securisez-votre-reseau-grace-aux-vpn-et-firewall