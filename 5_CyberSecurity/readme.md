# Lab 5 "IDS IPS"

Lab de test de solution de cybersécurité (IDS, IPS, scanner, OSINT)

## Lab local

### Snort

- [DONE] installation de snort

docker network create -d macvlan \
  --subnet=192.168.1.0/24 \
  --ip-range=192.168.1.40/25 \
  --gateway=192.168.1.1 \
macnethome

```
docker build . -t snort:testlab \
    && docker run --rm -i --name snort --net macnethome snort:testlab
```


- tutoriel vidéo utilisation snort : https://www.youtube.com/watch?v=S0M7cz4qQ-8

### Suricata

- [TODO]

### ARP Attack detector 

- [TODO] https://www.thepythoncode.com/article/detecting-arp-spoof-attacks-using-scapy

### lynis 

- [TODO] https://lindevs.com/install-lynis-on-ubuntu/

### container enforcing

- [TODO] scan breach and CVE on docker container :  https://www.grottedubarbu.fr/docker-scan-vuln-container/
- [TODO] enforcing container : https://github.com/OWASP/Docker-Security

### OSINT - sherlock (find data) : 

- [TODO] trouver des informations autour d'un pseudonyme avec sherlock :  https://www.it-connect.fr/recherchez-tous-les-comptes-correspondants-a-un-pseudo-avec-sherlock/
 

### maltrail 

- docker images : https://github.com/johackim/docker-maltrail

build :
````
podman build -t maltrail_test -f maltrail_ubuntu_dockerfile
````
run : 
````
podman run -d --name maltrail -p 8338:8338/tcp localhost/maltrail_test
````

### nmap (not working, raw socket not permitted error)

build :
````
podman build -t nmap_test -f nmap_dockerfile
````

run : 
````
podman run --rm -it instrumentisto/nmap -A -T4 127.0.0.1
````

### OWASP 

- [TODO] owasp in docker : https://www.grottedubarbu.fr/owasp-zap-docker/
- [TODO] ZAP with OWASP for testing web site security : OWASP Zed Attack Proxy

#### nettacker 

build : 
````
podman build -t nettacker_image -f nettacker_dockerfile
````

run :
````
podman run --rm -it --name nettacker -p 5000:5000/tcp  localhost/nettacker_image 
python3 nettacker.py --start-api --api-host 0.0.0.0
````

### network scanner 

- [TODO] https://www.thepythoncode.com/article/building-network-scanner-using-scapy

### port scanner 

- [TODO] https://www.thepythoncode.com/article/make-port-scanner-python

### ARP spoofer 

- [TODO] https://www.thepythoncode.com/article/building-arp-spoofer-using-scapy

## RAT in python 

- [TODO] https://github.com/Marten4n6/EvilOSX

## get password from google chrome 

- [TODO] https://www.thepythoncode.com/article/extract-chrome-passwords-python

## keylogger 

- [TODO] https://www.thepythoncode.com/article/write-a-keylogger-python
- [TODO] https://github.com/CounterfeitLlama/OSX-Keylogger

## worm

- [TODO] https://github.com/dbortiz/Worm/blob/master/worm.py

## sec tools list 

- [TODO] https://github.com/netdisciple?tab=repositories

## Scapy network attack script

- [TODO] https://github.com/BelaskerAdel/Few_Line_Code_Attacks


## SECURITY TEST

### podman 

- security test bench for podman : https://github.com/containers/podman-security-bench

# sources : 

## data gathering : 

- blog security trail  :  https://securitytrails.com/blog/information-gathering
- nmap cheatsheet : https://securitytrails.com/blog/nmap-cheat-sheet
- blog hacking : https://miloserdov.org/

snort :
- https://github.com/John-Lin/docker-snort/
- https://blog.cloudcontainers.net/story/2020%3A3%3A8%3A9%3A3314abd6618d81e42a577e7cf661f9ff#
- https://snort.org/downloads/#rule-downloads
- https://stackoverflow.com/questions/52411580/how-to-use-snort-to-detect-nmap-default-syn-scan
- https://frankfu.click/security/ids/how-to-detect-nmap-scan-using-snort/
- https://www.hackingarticles.in/detect-nmap-scan-using-snort/
- https://www.framboise314.fr/detection-dintrusion-ids-avec-suricata-sur-raspberry-pi/
- https://upcloud.com/resources/tutorials/install-snort-ubuntu

- apk add "--virtual" : https://stackoverflow.com/questions/46221063/what-is-build-deps-for-apk-add-virtual-command
- multi stage docker build : https://www.howtogeek.com/devops/what-are-multi-stage-docker-builds/