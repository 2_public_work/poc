# Privacy lab 

Testing some privacy tools

## Mac os privacy guide : 

- MacOS Hardening Guide : http://newosxbook.com/files/moxii3/AppendixA.pdf
- macOS-Security-and-Privacy-Guide : https://github.com/drduh/macOS-Security-and-Privacy-Guide

## Mac os Privacy tools :

- stronghold : https://github.com/alichtman/stronghold

## pihole 

- Block List for PiHole to block the connections made by the MiFit App to Chinese webservices : https://github.com/SweetSophia/mifitxiaomipiholelist