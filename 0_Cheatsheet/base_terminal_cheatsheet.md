# Most used command line

## Storage 

- see the hardware size : df -h
- see the size of a folder : du -hs /var/
- see the size of all subfolder : du -hs /var/*

## Process

- list all open process : ps -aux
- list all process in real time : top 

- find a process by a port (lsof) : losf -i:portnumber
- find a process by a port (netstat) : netstat -tlnp | grep portnumber
- kill it : kill $(lsof -t -i:portnumber)

- see process in background : jobs

## Network

- see open port on local machine (ss) : ss -ulnp
- see open port on local machine (lsof) : lsof -Pi | sort
- see open port on local machine (netstat) : 


‘‘‘

netstat -paunt

    -a : Tous les ports
    -t : Tous les ports TCP
    -u : Tous les ports UDP
    -l : Tous les ports en écoute
    -n : Affiche directement les IP. Pas de résolution de nom.
    -p : Affiche le nom du programme et le PID associé.

‘‘‘



- scan ports of a host : nmap -sS -sU -sV 192.168.1.101
- scan specific open port : nmap -p 80,8080 192.168.1.0-255