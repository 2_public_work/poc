# Lab 1 "core" :

Lab de test pour service de base (DHCP, DNS, NTP, sauvegarde, log)

## Lab local

### DHCP : 

- [TODO] monter un serveur DHCP "ics dhcp" pour infra conteneurisée
- [TODO] installer une interface web pour la gestion du service DHCP (stork) : https://gitlab.isc.org/isc-projects/stork

### DNS :

- [En cours] monter un serveur DNS avec bind (https://sinister.io/articles/podman-dns/)
- [En cours] monter un serveur pihole (https://sinister.io/articles/podman-dns/)
- [En cours] Faire du DoH (DNS over HTTPS) avec cloudflared (https://sinister.io/articles/podman-dns/)

- créer un pod pour les 3 services (DNS, cloudflared, pihole) : podman pod create --name dns --net slirp4netns -p '192.168.1.19:53:53/udp' -p '127.0.0.1:8080:80/tcp'  -p '127.0.0.1:8443:443/tcp'

- lancer le service DNS (serveur bind) : 
    - build : podman build -t bind_image -f bind_debian_dockerfile --format docker
    - run : podman run -d --name bind --pod dns -v '/var/home/core/.local/share/containers/storage/volumes/dns-volume/_data/bind/etc:/etc/bind:Z' --user 2001 localhost/bind_image

(the :Z option is used for labeling in Selinux)

- lancer le service cloudflared (pour DoH) : 
    - build : podman build -t cloudflared_image -f cloudflared_debian_dockerfile --format docker
    - run : podman run -d --name cloudflared --pod dns --user 2002 localhost/cloudflared_image

- lancer le service pihole :
    - build : pas de build, on récupère l'image depuis le dépôt dockerhub
    - run : podman run -d --name pihole --pod dns -v '/containers/run/pihole/etc/pihole:/etc/pihole:Z' -v '/containers/run/pihole/etc/dnsmasq.d:/etc/dnsmasq.d:Z' -e=ServerIP='192.168.1.19' -e=DNS1='127.0.0.1#5053' -e=DNS2='no' -e=IPv6='false' -e=TZ='Europe/Paris' -e=WEBPASSWORD='MY_STRONG_PASSWORD' pihole/pihole:latest

- [TODO] tester le serveur DNS technitium : https://technitium.com/dns/
- [TODO] DSN dynamique dockerisé : https://holory.fr/godns-client-dynamique-dns-ecrit-en-go/

### Cockpit

- [TODO] cockpit interface for podman container managing : https://www.tutorialworks.com/podman-monitoring-cockpit-fedora/
- [TODO] more to read about cokpit : https://opensource.com/article/20/11/cockpit-server-management

### syslog server

- [TODO] Surveiller les requêtes DNS 

### NTP

- [TODO] Monter un serveur NTP

### PKI

- [TODO] Créer une autorité de certification (infra PKI) : https://smallstep.com/blog/build-a-tiny-ca-with-raspberry-pi-yubikey/
- [TODO] utiliser un self signed certificate pour apache : https://lindevs.com/enable-https-for-apache-using-self-signed-ssl-certificate-on-ubuntu/


### Backup Restic

------

# cheatsheet 

## DNS 

### bind



### cloudflared



# Source : 

- Bonnes pratiques docker en production : https://w3blog.fr/2016/02/23/docker-securite-10-bonnes-pratiques/
- (guide/lab) docker container as DHCP server : https://gist.github.com/mikejoh/04978da4d52447ead7bdd045e878587d
- podman container DHCP leasing : https://www.redhat.com/sysadmin/leasing-ips-podman
- rootless in podman explained : https://www.tutorialworks.com/podman-rootless-volumes/
- how to set and use podman in rootless environnment : https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md
- dockerfile best practices : https://www.grottedubarbu.fr/best-practices-dockerfile/
- docker image scratch : https://www.grottedubarbu.fr/docker-image-scratch/
- installer un dns local sur rasp : https://raspberrytips.fr/serveur-dns-local-raspberry-pi/
- dns man page : https://linux.die.net/man/8/dnsmasq