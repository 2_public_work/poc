# Testing 

All tools, virtual env (container, vm, etc ...) for testing purpose.

## connectivity :

- [TODO] arping : https://lindevs.com/install-arping-on-raspberry-pi/

## website :

- [TODO] DSTP (website tester): https://lindevs.com/install-dstp-on-ubuntu/
- [TODO] vageta (website load testing) : https://lindevs.com/install-vegeta-on-ubuntu/
- [TODO] test ssl on web site : https://lindevs.com/install-testssl-sh-on-ubuntu/
- [TODO] test webserver performance (benchmark) : https://www.cyberciti.biz/tips/howto-performance-benchmarks-a-web-server.html
- [TODO] Testing proxy and http server with polygraph : https://www.web-polygraph.org/

#### bandwith : 

- [TODO] 

## software / programm testing 

- [TODO] testing programm with hyperfine : https://lindevs.com/install-hyperfine-on-raspberry-pi/

## hardware / system testing :

- [TODO] sysbench : https://lindevs.com/install-sysbench-on-raspberry-pi/
- [TODO] generate workload with stress : https://lindevs.com/install-stress-command-on-raspberry-pi/


# source : 