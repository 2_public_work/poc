# Lab 2 "AAA"

Lab de test de services d'Authentification, Autorisation et Accounting (LDAP, radius, proxy)

## Lab local

### LDAP (with bind) : 

- https://github.com/labbsr0x/docker-dns-bind9/tree/master/example/primary/bind/etc
- https://github.com/resyst-it/docker-bind9
- https://medium.com/nagoya-foundation/running-a-dns-server-in-docker-61cc2003e899
- https://www.golinuxcloud.com/configure-ldap-client-auth-ldap-server/
- https://coagul.org/drupal/article/installation-et-utilisation-openldap-sous-debian

### RADIUS

- [TODO]

### Proxy squid

- [Terminé] proxy squid avec podman

Construire l'image squid proxy avec podman :

‘‘‘
podman build -t squid_image -f squid_dockerfile
‘‘‘

lancer le service avec podman :

‘‘‘
podman run --name squid -p 3128:3128 -d localhost/squid_image 
‘‘‘

Ajouter les options suivantes pour monter les logs et le cache sur un volume (persistance) :
‘‘‘
--volume /tmp/squid/cache:/var/spool/squid \
--volume /tmp/squid/log:/var/log/squid \
‘‘‘

Testing (by watching logs) :

‘‘‘
podman exec -it squid tail -f /var/log/squid/access.log
‘‘‘

- [TODO] Authentification des restrictions squids avec identifiants ldap : https://www.cyberciti.biz/tips/howto-configure-squid-ldap-authentication.html