# Monitoring 

Lab de test de solutions de supervision, metrologie

Besoins en termes de monitoring : 
- Stockage (RGPD)
- Visualisation (Graph)
- Détection (algorithme, IA)
- Alerte (mail, systemstatus)

## Lab local

### TIG

- [TODO] monitoring livebox avec TIG : https://www.nas-forum.com/forum/topic/69631-tuto-monitoring-de-la-livebox4/
    - module sysbux pour récupération données livebox : https://github.com/rene-d/sysbus/blob/master/src/sysbus/sysbus.py

### supervisord

- [TODO] supervisord à tester :  http://supervisord.org/

### ELK

- [TODO] monitoring de conteneur avec ELK

### Prometheus 

- [TODO] monitoring de conteneur avec prometheus : https://sinister.io/articles/podman-prometheus-grafana-loki/#pmon-configs-promtail

### Glances

- [TODO] monitoring avec glance

### Libre NMS

- [TODO] supervision avec LibreNMS : https://www.librenms.org

# Source : 

- blog with some article about monitoring : https://computingforgeeks.com/category/monitoring/page/2/